package DivisibleSumPairs;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int[] arr = {1,2,3,4,5,6};
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < arr.length; i++) {
			list.add(arr[i]);
		}
		
		System.out.println("Enter the value of the divisor...");
		int k = in.nextInt();
		
		List<int[]> result = new ArrayList<int[]>();
		
		findUniqueCombination(list, result, k);
		displayList(result);
	}
	public static List<int[]> findUniqueCombination(List<Integer> inputList,List<int[] > result,int k){
		int inputListSize = inputList.size();
		for(int j = 1;j<inputListSize;j++) {
			int sum = inputList.get(0) + inputList.get(j);
			if(sum % k == 0) {
				int[] arr = new int[2];
				arr[0] = inputList.get(0);
				arr[1] = inputList.get(j);
				result.add(arr);
			}	
		}
		inputList.remove(0);
		if(inputListSize == 2) {
			return result;
		}
		else {
			return findUniqueCombination(inputList, result, k);
		}	
	}
	
	public static void displayList(List<int[]> list) {
		int length = list.size();
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print("{" + list.get(i)[0] + "," + list.get(i)[1]+ "}");
			}
			else {
				System.out.print("{" + list.get(i)[0] + "," + list.get(i)[1] + "}" + "  " );
			}
		}
	}
}
